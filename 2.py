#!/usr/bin/python
# -*- coding: utf-8 -*-
import pygame
import random
import math
from config import *
pygame.init()

# Screen

screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption('Bajrangi Babu')

pygame.display.set_icon(icon)

# Player1

playerImg = pygame.image.load('bajrangismile.png')
playerX = 400
playerY = 570
playerX_change = 0
playerY_change = 0

# Player2

playerImg = pygame.image.load('bajrangismile.png')
player2X = 400
player2Y = 10
player2X_change = 0
player2Y_change = 0

# Fixed Obstacle 1-1

Obst1_1Img = pygame.image.load('couple.png')
enemyX_11 = random.randint(0, 800)
enemyY_11 = 500

# Fixed Obstacle 1-2

Obst1_2Img = pygame.image.load('couple.png')
enemyX_12 = random.randint(0, 800)
enemyY_12 = 500

# Fixed Obstacle 1-3

Obst1_3Img = pygame.image.load('couple.png')
enemyX_13 = random.randint(0, 800)
enemyY_13 = 500

# Fixed Obstacle 2-1

Obst2_1Img = pygame.image.load('couple.png')
enemyX_21 = random.randint(0, 800)
enemyY_21 = 350

# Fixed Obstacle 2-2

Obst2_2Img = pygame.image.load('couple.png')
enemyX_22 = random.randint(0, 800)
enemyY_22 = 350

# Fixed Obstacle 2-3

Obst2_3Img = pygame.image.load('couple.png')
enemyX_23 = random.randint(0, 800)
enemyY_23 = 350

# Fixed Obstacle 2-4

Obst2_4Img = pygame.image.load('couple.png')
enemyX_24 = random.randint(0, 800)
enemyY_24 = 350

# Fixed Obstacle 3-1

Obst3_1Img = pygame.image.load('couple.png')
enemyX_31 = random.randint(0, 800)
enemyY_31 = 200

# Fixed Obstacle 3-2

Obst3_2Img = pygame.image.load('couple.png')
enemyX_32 = random.randint(0, 800)
enemyY_32 = 200

# Fixed Obstacle 3-3

Obst3_3Img = pygame.image.load('couple.png')
enemyX_33 = random.randint(0, 800)
enemyY_33 = 200

# Fixed Obstacle 4-1

Obst4_1Img = pygame.image.load('couple.png')
enemyX_41 = random.randint(0, 800)
enemyY_41 = 50

# Fixed Obstacle 4-2

Obst4_2Img = pygame.image.load('couple.png')
enemyX_42 = random.randint(0, 800)
enemyY_42 = 50

# Fixed Obstacle 4-3

Obst4_3Img = pygame.image.load('couple.png')
enemyX_43 = random.randint(0, 800)
enemyY_43 = 50

# Fixed Obstacle 4-4

Obst4_4Img = pygame.image.load('couple.png')
enemyX_44 = random.randint(0, 800)
enemyY_44 = 50

# Moving Obstacle 1-1

Girl1_1Img = pygame.image.load('enemy.png')
GirlX_11 = random.randint(0, 800)
GirlY_11 = 425
GirlX_change = 0.1

# Moving Obstacle 1-2

Girl1_2Img = pygame.image.load('enemy.png')
GirlX_12 = random.randint(0, 800)
GirlY_12 = 425
GirlX_change = 0.1

# Moving Obstacle 1-3

Girl1_3Img = pygame.image.load('enemy.png')
GirlX_13 = random.randint(0, 800)
GirlY_13 = 425
GirlX_change = 0.1

# Moving Obstacle 2-1

Girl2_1Img = pygame.image.load('enemy.png')
GirlX_21 = random.randint(0, 800)
GirlY_21 = 275
GirlX_change = 0.1

# Moving Obstacle 2-2

Girl2_2Img = pygame.image.load('enemy.png')
GirlX_22 = random.randint(0, 800)
GirlY_22 = 275
GirlX_change = 0.1

# Moving Obstacle 2-3

Girl2_3Img = pygame.image.load('enemy.png')
GirlX_23 = random.randint(0, 800)
GirlY_23 = 275
GirlX_change = 0.1

# Moving Obstacle 3-1

Girl3_1Img = pygame.image.load('enemy.png')
GirlX_31 = random.randint(0, 800)
GirlY_31 = 125
GirlX_change = 0.1

# Moving Obstacle 3-2

Girl3_2Img = pygame.image.load('enemy.png')
GirlX_32 = random.randint(0, 800)
GirlY_32 = 125
GirlX_change = 0.1

# Moving Obstacle 3-3

Girl3_3Img = pygame.image.load('enemy.png')
GirlX_33 = random.randint(0, 800)
GirlY_33 = 125
GirlX_change = 0.1


# player blit

def player(x, y):
    screen.blit(playerImg, (x, y))


def player2(x, y):
    screen.blit(playerImg, (x, y))


def enemy_11(x, y):
    screen.blit(Obst1_1Img, (x, y))


def enemy_12(x, y):
    screen.blit(Obst1_2Img, (x, y))


def enemy_13(x, y):
    screen.blit(Obst1_3Img, (x, y))


def enemy_21(x, y):
    screen.blit(Obst2_1Img, (x, y))


def enemy_22(x, y):
    screen.blit(Obst2_2Img, (x, y))


def enemy_23(x, y):
    screen.blit(Obst2_3Img, (x, y))


def enemy_24(x, y):
    screen.blit(Obst2_4Img, (x, y))


def enemy_31(x, y):
    screen.blit(Obst3_1Img, (x, y))


def enemy_32(x, y):
    screen.blit(Obst3_2Img, (x, y))


def enemy_33(x, y):
    screen.blit(Obst3_3Img, (x, y))


def enemy_41(x, y):
    screen.blit(Obst4_1Img, (x, y))


def enemy_42(x, y):
    screen.blit(Obst4_2Img, (x, y))


def enemy_43(x, y):
    screen.blit(Obst4_3Img, (x, y))


def enemy_44(x, y):
    screen.blit(Obst4_4Img, (x, y))


def girl_11(x, y):
    screen.blit(Girl1_1Img, (x, y))


def girl_12(x, y):
    screen.blit(Girl1_2Img, (x, y))


def girl_13(x, y):
    screen.blit(Girl1_3Img, (x, y))


def girl_21(x, y):
    screen.blit(Girl2_1Img, (x, y))


def girl_22(x, y):
    screen.blit(Girl2_2Img, (x, y))


def girl_23(x, y):
    screen.blit(Girl2_3Img, (x, y))


def girl_31(x, y):
    screen.blit(Girl3_1Img, (x, y))


def girl_32(x, y):
    screen.blit(Girl3_2Img, (x, y))


def girl_33(x, y):
    screen.blit(Girl3_3Img, (x, y))


# collision

def collision11(
    playerX,
    playerY,
    enemyX_11,
    enemyY_11,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_11, 2)
                         + math.pow(playerY - enemyY_11, 2))
    if distance < 45:
        return True
    else:
        return False


def collision12(
    playerX,
    playerY,
    enemyX_12,
    enemyY_12,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_12, 2)
                         + math.pow(playerY - enemyY_12, 2))
    if distance < 45:
        return True
    else:
        return False


def collision13(
    playerX,
    playerY,
    enemyX_13,
    enemyY_13,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_13, 2)
                         + math.pow(playerY - enemyY_13, 2))
    if distance < 45:
        return True
    else:
        return False


def collision21(
    playerX,
    playerY,
    enemyX_21,
    enemyY_21,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_21, 2)
                         + math.pow(playerY - enemyY_21, 2))
    if distance < 45:
        return True
    else:
        return False


def collision22(
    playerX,
    playerY,
    enemyX_22,
    enemyY_22,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_22, 2)
                         + math.pow(playerY - enemyY_22, 2))
    if distance < 45:
        return True
    else:
        return False


def collision23(
    playerX,
    playerY,
    enemyX_23,
    enemyY_23,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_23, 2)
                         + math.pow(playerY - enemyY_23, 2))
    if distance < 45:
        return True
    else:
        return False


def collision24(
    playerX,
    playerY,
    enemyX_24,
    enemyY_24,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_24, 2)
                         + math.pow(playerY - enemyY_24, 2))
    if distance < 45:
        return True
    else:
        return False


def collision31(
    playerX,
    playerY,
    enemyX_31,
    enemyY_31,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_31, 2)
                         + math.pow(playerY - enemyY_31, 2))
    if distance < 45:
        return True
    else:
        return False


def collision32(
    playerX,
    playerY,
    enemyX_32,
    enemyY_32,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_32, 2)
                         + math.pow(playerY - enemyY_32, 2))
    if distance < 45:
        return True
    else:
        return False


def collision33(
    playerX,
    playerY,
    enemyX_33,
    enemyY_33,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_33, 2)
                         + math.pow(playerY - enemyY_33, 2))
    if distance < 45:
        return True
    else:
        return False


def collision41(
    playerX,
    playerY,
    enemyX_41,
    enemyY_41,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_41, 2)
                         + math.pow(playerY - enemyY_41, 2))
    if distance < 45:
        return True
    else:
        return False


def collision42(
    playerX,
    playerY,
    enemyX_42,
    enemyY_42,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_42, 2)
                         + math.pow(playerY - enemyY_42, 2))
    if distance < 45:
        return True
    else:
        return False


def collision43(
    playerX,
    playerY,
    enemyX_43,
    enemyY_43,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_43, 2)
                         + math.pow(playerY - enemyY_43, 2))
    if distance < 45:
        return True
    else:
        return False


def collision44(
    playerX,
    playerY,
    enemyX_44,
    enemyY_44,
    ):
    distance = math.sqrt(math.pow(playerX - enemyX_44, 2)
                         + math.pow(playerY - enemyY_44, 2))
    if distance < 45:
        return True
    else:
        return False


def collision51(
    playerX,
    playerY,
    GirlX_11,
    GirlY_11,
    ):
    distance = math.sqrt(math.pow(playerX - GirlX_11, 2)
                         + math.pow(playerY - GirlY_11, 2))
    if distance < 45:
        return True
    else:
        return False


def collision52(
    playerX,
    playerY,
    GirlX_12,
    GirlY_12,
    ):
    distance = math.sqrt(math.pow(playerX - GirlX_12, 2)
                         + math.pow(playerY - GirlY_12, 2))
    if distance < 45:
        return True
    else:
        return False


def collision53(
    playerX,
    playerY,
    GirlX_13,
    GirlY_13,
    ):
    distance = math.sqrt(math.pow(playerX - GirlX_13, 2)
                         + math.pow(playerY - GirlY_13, 2))
    if distance < 45:
        return True
    else:
        return False


def collision61(
    playerX,
    playerY,
    GirlX_21,
    GirlY_21,
    ):
    distance = math.sqrt(math.pow(playerX - GirlX_21, 2)
                         + math.pow(playerY - GirlY_21, 2))
    if distance < 45:
        return True
    else:
        return False


def collision62(
    playerX,
    playerY,
    GirlX_22,
    GirlY_22,
    ):
    distance = math.sqrt(math.pow(playerX - GirlX_22, 2)
                         + math.pow(playerY - GirlY_22, 2))
    if distance < 45:
        return True
    else:
        return False


def collision63(
    playerX,
    playerY,
    GirlX_23,
    GirlY_23,
    ):
    distance = math.sqrt(math.pow(playerX - GirlX_23, 2)
                         + math.pow(playerY - GirlY_23, 2))
    if distance < 45:
        return True
    else:
        return False


def collision71(
    playerX,
    playerY,
    GirlX_31,
    GirlY_31,
    ):
    distance = math.sqrt(math.pow(playerX - GirlX_31, 2)
                         + math.pow(playerY - GirlY_31, 2))
    if distance < 45:
        return True
    else:
        return False


def collision72(
    playerX,
    playerY,
    GirlX_32,
    GirlY_32,
    ):
    distance = math.sqrt(math.pow(playerX - GirlX_32, 2)
                         + math.pow(playerY - GirlY_32, 2))
    if distance < 45:
        return True
    else:
        return False


def collision73(
    playerX,
    playerY,
    GirlX_33,
    GirlY_33,
    ):
    distance = math.sqrt(math.pow(playerX - GirlX_33, 2)
                         + math.pow(playerY - GirlY_33, 2))
    if distance < 45:
        return True
    else:
        return False


# collision with player2

def collision11(
    player2X,
    player2Y,
    enemyX_11,
    enemyY_11,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_11, 2)
                         + math.pow(player2Y - enemyY_11, 2))
    if distance < 45:
        return True
    else:
        return False


def collision12(
    player2X,
    player2Y,
    enemyX_12,
    enemyY_12,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_12, 2)
                         + math.pow(player2Y - enemyY_12, 2))
    if distance < 45:
        return True
    else:
        return False


def collision13(
    player2X,
    player2Y,
    enemyX_13,
    enemyY_13,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_13, 2)
                         + math.pow(player2Y - enemyY_13, 2))
    if distance < 45:
        return True
    else:
        return False


def collision21(
    player2X,
    player2Y,
    enemyX_21,
    enemyY_21,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_21, 2)
                         + math.pow(player2Y - enemyY_21, 2))
    if distance < 45:
        return True
    else:
        return False


def collision22(
    player2X,
    player2Y,
    enemyX_22,
    enemyY_22,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_22, 2)
                         + math.pow(player2Y - enemyY_22, 2))
    if distance < 45:
        return True
    else:
        return False


def collision23(
    player2X,
    player2Y,
    enemyX_23,
    enemyY_23,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_23, 2)
                         + math.pow(player2Y - enemyY_23, 2))
    if distance < 45:
        return True
    else:
        return False


def collision24(
    player2X,
    player2Y,
    enemyX_24,
    enemyY_24,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_24, 2)
                         + math.pow(player2Y - enemyY_24, 2))
    if distance < 45:
        return True
    else:
        return False


def collision31(
    player2X,
    player2Y,
    enemyX_31,
    enemyY_31,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_31, 2)
                         + math.pow(player2Y - enemyY_31, 2))
    if distance < 45:
        return True
    else:
        return False


def collision32(
    player2X,
    player2Y,
    enemyX_32,
    enemyY_32,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_32, 2)
                         + math.pow(player2Y - enemyY_32, 2))
    if distance < 45:
        return True
    else:
        return False


def collision33(
    player2X,
    player2Y,
    enemyX_33,
    enemyY_33,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_33, 2)
                         + math.pow(player2Y - enemyY_33, 2))
    if distance < 45:
        return True
    else:
        return False


def collision41(
    player2X,
    player2Y,
    enemyX_41,
    enemyY_41,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_41, 2)
                         + math.pow(player2Y - enemyY_41, 2))
    if distance < 45:
        return True
    else:
        return False


def collision42(
    player2X,
    player2Y,
    enemyX_42,
    enemyY_42,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_42, 2)
                         + math.pow(player2Y - enemyY_42, 2))
    if distance < 45:
        return True
    else:
        return False


def collision43(
    player2X,
    player2Y,
    enemyX_43,
    enemyY_43,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_43, 2)
                         + math.pow(player2Y - enemyY_43, 2))
    if distance < 45:
        return True
    else:
        return False


def collision44(
    player2X,
    player2Y,
    enemyX_44,
    enemyY_44,
    ):
    distance = math.sqrt(math.pow(player2X - enemyX_44, 2)
                         + math.pow(player2Y - enemyY_44, 2))
    if distance < 45:
        return True
    else:
        return False


def collision51(
    player2X,
    player2Y,
    GirlX_11,
    GirlY_11,
    ):
    distance = math.sqrt(math.pow(player2X - GirlX_11, 2)
                         + math.pow(player2Y - GirlY_11, 2))
    if distance < 45:
        return True
    else:
        return False


def collision52(
    player2X,
    player2Y,
    GirlX_12,
    GirlY_12,
    ):
    distance = math.sqrt(math.pow(player2X - GirlX_12, 2)
                         + math.pow(player2Y - GirlY_12, 2))
    if distance < 45:
        return True
    else:
        return False


def collision53(
    player2X,
    player2Y,
    GirlX_13,
    GirlY_13,
    ):
    distance = math.sqrt(math.pow(player2X - GirlX_13, 2)
                         + math.pow(player2Y - GirlY_13, 2))
    if distance < 45:
        return True
    else:
        return False


def collision61(
    player2X,
    player2Y,
    GirlX_21,
    GirlY_21,
    ):
    distance = math.sqrt(math.pow(player2X - GirlX_21, 2)
                         + math.pow(player2Y - GirlY_21, 2))
    if distance < 45:
        return True
    else:
        return False


def collision62(
    player2X,
    player2Y,
    GirlX_22,
    GirlY_22,
    ):
    distance = math.sqrt(math.pow(player2X - GirlX_22, 2)
                         + math.pow(player2Y - GirlY_22, 2))
    if distance < 45:
        return True
    else:
        return False


def collision63(
    player2X,
    player2Y,
    GirlX_23,
    GirlY_23,
    ):
    distance = math.sqrt(math.pow(player2X - GirlX_23, 2)
                         + math.pow(player2Y - GirlY_23, 2))
    if distance < 45:
        return True
    else:
        return False


def collision71(
    player2X,
    player2Y,
    GirlX_31,
    GirlY_31,
    ):
    distance = math.sqrt(math.pow(player2X - GirlX_31, 2)
                         + math.pow(player2Y - GirlY_31, 2))
    if distance < 45:
        return True
    else:
        return False


def collision72(
    player2X,
    player2Y,
    GirlX_32,
    GirlY_32,
    ):
    distance = math.sqrt(math.pow(player2X - GirlX_32, 2)
                         + math.pow(player2Y - GirlY_32, 2))
    if distance < 45:
        return True
    else:
        return False


def collision73(
    player2X,
    player2Y,
    GirlX_33,
    GirlY_33,
    ):
    distance = math.sqrt(math.pow(player2X - GirlX_33, 2)
                         + math.pow(player2Y - GirlY_33, 2))
    if distance < 45:
        return True
    else:
        return False


# exit condition

open = True
while open:
    screen.fill((0, 0, 0))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            open = False

     # movements ==

    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_LEFT:
            playerX_change = -0.2
        if event.key == pygame.K_RIGHT:
            playerX_change = 0.2
        if event.key == pygame.K_UP:
            playerY_change = -0.2
        if event.key == pygame.K_DOWN:
            playerY_change = 0.2

 # Borders................

    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_a:
            player2X_change = -0.2
        if event.key == pygame.K_d:
            player2X_change = 0.2
        if event.key == pygame.K_w:
            player2Y_change = -0.2
        if event.key == pygame.K_s:
            player2Y_change = 0.2

    if event.type == pygame.KEYUP:
        if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
            playerX_change = 0
        if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
            playerY_change = 0

    if event.type == pygame.KEYUP:
        if event.key == pygame.K_a or event.key == pygame.K_d:
            player2X_change = 0
        if event.key == pygame.K_w or event.key == pygame.K_s:
            player2Y_change = 0

    playerX += playerX_change
    playerY += playerY_change

    player2X += player2X_change
    player2Y += player2Y_change

    GirlX_11 += GirlX_change
    GirlX_12 += GirlX_change
    GirlX_13 += GirlX_change
    GirlX_21 += GirlX_change
    GirlX_22 += GirlX_change
    GirlX_23 += GirlX_change
    GirlX_31 += GirlX_change
    GirlX_32 += GirlX_change
    GirlX_33 += GirlX_change

    score = 0

    if playerX <= 0:
        playerX = 0
    elif playerX >= 768:
        playerX = 768
    if playerY <= 0:
        playerY = 0
    elif playerY >= 568:
        playerY = 568
    if player2X <= 0:
        player2X = 0
    elif player2X >= 768:
        player2X = 768
    if player2Y <= 0:
        player2Y = 0
    elif player2Y >= 568:
        player2Y = 568
    if GirlX_11 >= 800:
        GirlX_11 = 0
    if GirlX_12 >= 800:
        GirlX_12 = 0
    if GirlX_13 >= 800:
        GirlX_13 = 0
    if GirlX_21 >= 800:
        GirlX_21 = 0
    if GirlX_22 >= 800:
        GirlX_22 = 0
    if GirlX_23 >= 800:
        GirlX_23 = 0
    if GirlX_31 >= 800:
        GirlX_31 = 0
    if GirlX_32 >= 800:
        GirlX_32 = 0
    if GirlX_33 >= 800:
        GirlX_33 = 0

 # COLLISION....

    boom1 = collision11(playerX, playerY, enemyX_11, enemyY_11)
    if boom1:
        playerX = 400
        playerY = 600
    boom2 = collision12(playerX, playerY, enemyX_12, enemyY_12)
    if boom2:
        playerX = 400
        playerY = 600
    boom3 = collision13(playerX, playerY, enemyX_13, enemyY_13)
    if boom3:
        playerX = 400
        playerY = 600
    boom4 = collision21(playerX, playerY, enemyX_21, enemyY_21)
    if boom4:
        playerX = 400
        playerY = 600
    boom5 = collision22(playerX, playerY, enemyX_22, enemyY_22)
    if boom5:
        playerX = 400
        playerY = 600
    boom6 = collision23(playerX, playerY, enemyX_23, enemyY_23)
    if boom6:
        playerX = 400
        playerY = 600
    boom7 = collision24(playerX, playerY, enemyX_24, enemyY_24)
    if boom7:
        playerX = 400
        playerY = 600
    boom8 = collision31(playerX, playerY, enemyX_31, enemyY_31)
    if boom8:
        playerX = 400
        playerY = 600
    boom9 = collision32(playerX, playerY, enemyX_32, enemyY_32)
    if boom9:
        playerX = 400
        playerY = 600
    boom10 = collision33(playerX, playerY, enemyX_33, enemyY_33)
    if boom10:
        playerX = 400
        playerY = 600
    boom11 = collision41(playerX, playerY, enemyX_41, enemyY_41)
    if boom11:
        playerX = 400
        playerY = 600
    boom12 = collision42(playerX, playerY, enemyX_42, enemyY_42)
    if boom12:
        playerX = 400
        playerY = 600
    boom13 = collision43(playerX, playerY, enemyX_43, enemyY_43)
    if boom13:
        playerX = 400
        playerY = 600
    boom14 = collision44(playerX, playerY, enemyX_44, enemyY_44)
    if boom14:
        playerX = 400
        playerY = 600
    boom15 = collision51(playerX, playerY, GirlX_11, GirlY_11)
    if boom15:
        playerX = 400
        playerY = 600
    boom16 = collision52(playerX, playerY, GirlX_12, GirlY_12)
    if boom16:
        playerX = 400
        playerY = 600
    boom17 = collision53(playerX, playerY, GirlX_13, GirlY_13)
    if boom17:
        playerX = 400
        playerY = 600
    boom18 = collision61(playerX, playerY, GirlX_21, GirlY_21)
    if boom18:
        playerX = 400
        playerY = 600
    boom19 = collision62(playerX, playerY, GirlX_22, GirlY_22)
    if boom19:
        playerX = 400
        playerY = 600
    boom20 = collision63(playerX, playerY, GirlX_23, GirlY_23)
    if boom20:
        playerX = 400
        playerY = 600
    boom21 = collision71(playerX, playerY, GirlX_31, GirlY_31)
    if boom21:
        playerX = 400
        playerY = 600
    boom22 = collision72(playerX, playerY, GirlX_32, GirlY_32)
    if boom22:
        playerX = 400
        playerY = 600
    boom23 = collision73(playerX, playerY, GirlX_33, GirlY_33)
    if boom23:
        playerX = 400
        playerY = 600

 # COLLISION....WITH PLAYER2

    boomb1 = collision11(player2X, player2Y, enemyX_11, enemyY_11)
    if boomb1:
        player2X = 400
        player2Y = 10
    boom2 = collision12(player2X, player2Y, enemyX_12, enemyY_12)
    if boom2:
        player2X = 400
        player2Y = 10
    boom3 = collision13(player2X, player2Y, enemyX_13, enemyY_13)
    if boom3:
        player2X = 400
        player2Y = 10
    boom4 = collision21(player2X, player2Y, enemyX_21, enemyY_21)
    if boom4:
        player2X = 400
        player2Y = 10
    boom5 = collision22(player2X, player2Y, enemyX_22, enemyY_22)
    if boom5:
        player2X = 400
        player2Y = 10
    boom6 = collision23(player2X, player2Y, enemyX_23, enemyY_23)
    if boom6:
        player2X = 400
        player2Y = 10
    boom7 = collision24(player2X, player2Y, enemyX_24, enemyY_24)
    if boom7:
        player2X = 400
        player2Y = 10
    boom8 = collision31(player2X, player2Y, enemyX_31, enemyY_31)
    if boom8:
        player2X = 400
        player2Y = 10
    boom9 = collision32(player2X, player2Y, enemyX_32, enemyY_32)
    if boom9:
        player2X = 400
        player2Y = 10
    boomb10 = collision33(player2X, player2Y, enemyX_33, enemyY_33)
    if boomb10:
        player2X = 400
        player2Y = 10
    boomb11 = collision41(player2X, player2Y, enemyX_41, enemyY_41)
    if boomb11:
        player2X = 400
        player2Y = 10
    boomb12 = collision42(player2X, player2Y, enemyX_42, enemyY_42)
    if boomb12:
        player2X = 400
        player2Y = 10
    boomb13 = collision43(player2X, player2Y, enemyX_43, enemyY_43)
    if boomb13:
        player2X = 400
        player2Y = 10
    boomb14 = collision44(player2X, player2Y, enemyX_44, enemyY_44)
    if boomb14:
        player2X = 400
        player2Y = 10
    boomb15 = collision51(player2X, player2Y, GirlX_11, GirlY_11)
    if boomb15:
        player2X = 400
        player2Y = 10
    boomb16 = collision52(player2X, player2Y, GirlX_12, GirlY_12)
    if boomb16:
        player2X = 400
        player2Y = 10
    boomb17 = collision53(player2X, player2Y, GirlX_13, GirlY_13)
    if boomb17:
        player2X = 400
        player2Y = 10
    boomb18 = collision61(player2X, player2Y, GirlX_21, GirlY_21)
    if boomb18:
        player2X = 400
        player2Y = 10
    boomb19 = collision62(player2X, player2Y, GirlX_22, GirlY_22)
    if boomb19:
        player2X = 400
        player2Y = 10
    boom20 = collision63(player2X, player2Y, GirlX_23, GirlY_23)
    if boom20:
        player2X = 400
        player2Y = 10
    boom21 = collision71(player2X, player2Y, GirlX_31, GirlY_31)
    if boom21:
        player2X = 400
        player2Y = 10
    boom22 = collision72(player2X, player2Y, GirlX_32, GirlY_32)
    if boom22:
        player2X = 400
        player2Y = 10
    boom23 = collision73(player2X, player2Y, GirlX_33, GirlY_33)
    if boom23:
        player2X = 400
        player2Y = 10

 # scoring
     # #if playerY:

    player(playerX, playerY)
    player2(player2X, player2Y)
    enemy_11(enemyX_11, enemyY_11)
    enemy_12(enemyX_12, enemyY_12)
    enemy_13(enemyX_13, enemyY_13)
    enemy_21(enemyX_21, enemyY_21)
    enemy_22(enemyX_22, enemyY_22)
    enemy_23(enemyX_23, enemyY_23)
    enemy_24(enemyX_24, enemyY_24)
    enemy_31(enemyX_31, enemyY_31)
    enemy_32(enemyX_32, enemyY_32)
    enemy_33(enemyX_33, enemyY_33)
    enemy_41(enemyX_41, enemyY_41)
    enemy_42(enemyX_42, enemyY_42)
    enemy_43(enemyX_43, enemyY_43)
    enemy_44(enemyX_44, enemyY_44)
    girl_11(GirlX_11, GirlY_11)
    girl_12(GirlX_12, GirlY_12)
    girl_13(GirlX_13, GirlY_13)
    girl_21(GirlX_21, GirlY_21)
    girl_22(GirlX_22, GirlY_22)
    girl_23(GirlX_23, GirlY_23)
    girl_31(GirlX_31, GirlY_31)
    girl_32(GirlX_32, GirlY_32)
    girl_33(GirlX_33, GirlY_33)

    pygame.display.update()
